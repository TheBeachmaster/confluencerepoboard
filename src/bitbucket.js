import { fetch } from "@forge/api";

const repoUser = process.env.USERNAME;
const repoPassword = process.env.PASSWORD;

const bbBasePath = "https://api.bitbucket.org/2.0";

const authHeaders = {
  Authorization:
    "Basic " +
    Buffer.from( repoUser + ":" + repoPassword).toString("base64"),
};

export async function fetchUserRepoInfo(type) {
  const reponame = "confluencerepoboard";
  const workspace = "thebeachmaster";

  let data = [];

  switch (type) {
    case "pr":
      const getAllPRs = await getAllRepos(workspace, reponame, "pullrequests/");
      data = getAllPRs;
      break;
    case "pipe":
      const getAllPipelines = await getAllRepos(
        workspace,
        reponame,
        "pipelines/"
      );
      data = getAllPipelines;
      break;
    case "team":
      const getTeam = await getWSTeam(workspace);
      data = getTeam;
      break;
    default:
      break;
  }

  return data;
}

async function getAllRepos(workspace, reponame, path) {
  const allWorkSpacesPath = `${bbBasePath}/repositories/${workspace}/${reponame}/${path}`;
  console.log("Path: ", allWorkSpacesPath);
  console.log("Getting Data");
  console.log("Headers: ", authHeaders);
  const requestData = await fetch(allWorkSpacesPath, {
    method: "GET",
    headers: {
      authorization: authHeaders.Authorization,
    },
  });
  if (requestData.ok) {
    const jsonResponse = await requestData.json();
    console.log(JSON.stringify(jsonResponse.values), null, 2);
    return jsonResponse.values;
  } else {
    console.log("Error: ", requestData.statusText);
    let r = [];
    return r;
  }
}

async function getWSTeam(workspace) {
  const allWorkSpacesPath = `${bbBasePath}/workspaces/${workspace}/members`;
  console.log("Path: ", allWorkSpacesPath);
  console.log("Getting Data");
  console.log("Headers: ", authHeaders);
  const requestData = await fetch(allWorkSpacesPath, {
    method: "GET",
    headers: {
      authorization: authHeaders.Authorization,
    },
  });
  if (requestData.ok) {
    const jsonResponse = await requestData.json();
    console.log(JSON.stringify(jsonResponse.values), null, 2);
    return jsonResponse.values;
  } else {
    console.log("Error: ", requestData.statusText);
    let r = [];
    return r;
  }
}
