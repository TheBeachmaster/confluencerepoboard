import ForgeUI, { render, Fragment, Macro, Text, ConfigForm, TextField, Table, Head, Cell, Row, useAction, useConfig } from "@forge/ui";
import { fetchUserRepoInfo } from "./bitbucket";

const App = () => {

  const config = useConfig();

  const [prs] = useAction(repo => repo, async () => {
    return await fetchUserRepoInfo("pr");
  });
  const [pipels] = useAction(repo => repo, async () => {
    return await fetchUserRepoInfo("pipe");
  });
  const [teams] = useAction(repo => repo, async () => {
    return await fetchUserRepoInfo("team");
  });
  console.log("Pull Req Result: ", prs);
  console.log("Pipelines Req Result: ", pipels);
  console.log("Teams Result: ", teams);
  return (
    <Table>
      <Head>
        <Cell>
          <Text content="Repo Info" />
        </Cell>
      </Head>
      <Row>
        <Cell>
          <Text content="Pull Requests" />
        </Cell>
      </Row>
      {prs.map((prsMappped, index) => (
        <Row>
          <Cell>
            <Text content={prsMappped.title} />
          </Cell>
        </Row>
      ))
      }
      <Row>
        <Cell>
          <Text content="Piplelines State" />
        </Cell>
      </Row>
      {pipels.map((pplMappped, index) => (
        <Row>
          <Cell>
            <Text content={pplMappped.state.name} />
          </Cell>
        </Row>
      ))
      }
      <Row>
        <Cell>
          <Text content="Team" />
        </Cell>
      </Row>
      {teams.map((teamsMappped, index) => (
        <Row>
          <Cell>
            <Text content={teamsMappped.user.display_name} />
          </Cell>
        </Row>
      ))
      }
    </Table>

  );
};

const Config = () => {
  return (
    <ConfigForm>
      <TextField name="repo" label="Repo Name" />
      <TextField name="workspace" label="Workspace" />
    </ConfigForm>
  );
};

export const run = render(
  <Macro
    app={<App />}
    config={<Config />}
  />
);
